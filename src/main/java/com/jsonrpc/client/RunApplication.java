package com.jsonrpc.client;

import java.net.URL;
import java.util.List;

import com.jsonrpc.pub.dto.Address;
import com.jsonrpc.pub.dto.WebUser;
import com.jsonrpc.pub.interfaces.WebUserService;

public class RunApplication  {

	public static void main(String[] args) {
		RunApplication main = new RunApplication();
//		main.createWebUser();
		main.find();
	}
	
	private void createWebUser() {
		try {
			WebUserService service = new WebUserClient().getWebUserClient(
										new URL("http://localhost:8080/json-rpc/web-user-service"));
			
			WebUser webUser = WebUser.builder()
									.username("houwaey")
									.password("mypassword")
									.email("test@email.com")
									.address(Address.builder()
												.city("Tiaong")
												.state("Quezon")
												.country("Philippines")
											.build())
								.build();
			boolean result = service.createWebUser(webUser);
			System.out.println("result: " + result);
		} catch (Exception e) {
			System.out.println("Message: " + e.getMessage());
		} finally {
			System.exit(0);
		}
	}
	
	private void find() {
		try {
			WebUserService service = new WebUserClient().getWebUserClient(
										new URL("http://localhost:8080/json-rpc/web-user-service"));
			
			WebUser webUser = service.findOneById(2);
			System.out.println(webUser.toString());
			
			webUser = service.findOneByUsername("houwaey");
			System.out.println(webUser.toString());
			
			List<WebUser> list = service.findAll();
			System.out.println(list);
		} catch (Exception e) {
			System.out.println("Message: " + e.getMessage());
			e.printStackTrace();
		} finally {
			System.exit(0);
		}
	}

}