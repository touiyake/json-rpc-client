package com.jsonrpc.client;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;

public class ClientFactory<T> {
	
	protected Class<T> klazz = null;

	protected T getWebUserClient(URL url) throws MalformedURLException {
		JsonRpcHttpClient client = new JsonRpcHttpClient(url, new HashMap<String, String>());
		return ProxyUtil.createClientProxy(this.getClass().getClassLoader(), this.klazz, client);
	}

}