package com.jsonrpc.client;

import com.jsonrpc.pub.interfaces.WebUserService;

public class WebUserClient extends ClientFactory<WebUserService> {

	public WebUserClient() {
		super.klazz = WebUserService.class;
	}
	
}
